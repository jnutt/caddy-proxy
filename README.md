# Caddy Proxy

[Caddy](https://caddyserver.com) is a lightweight HTTP server.

Sometimes I'm testing stuff locally and I want to throw a lightweight HTTPS proxy in front of it. Caddy is perfect for that.

1. [Install Caddy](https://caddyserver.com/docs/install).
1. Generate your certificate using `mkcerts.sh`. Make sure you set the FQDN when prompted. `localhost` is probably fine. If you use some other hostname, make sure to set your `/etc/hosts` accordingly.
1. Type `caddy run` in your terminal in the same directory as `Caddyfile`.
