#!/bin/bash

openssl req  \
  -nodes -new -x509  \
  -keyout secure.local.key \
  -out secure.local.cert
